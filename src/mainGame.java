import java.util.*;

public class mainGame {
	static Scanner a = new Scanner(System.in);

	static player p1 = new player();
	static player p2 = new player();

	static int playerTurn = 1;

	static boolean onGoing = false;

	public static void main(String[] args) {
		System.out.println("||||||||||||||||");
		System.out.println("||CONNECT FOUR||");
		System.out.println("||||||||||||||||");
		showMenu();
		int election = a.nextInt();
		while (election > 3 || election < 1) {
			System.out.println("Please, chose a valid option");
		}
		switch (election) {
		case 1:
			play();
			break;
		case 2:
			options();
			break;
		}
		System.out.println("Tanks for playing the game!\nSee you soon :)");
	}

	private static void play() {
		System.out.println("The game has been started!");
		onGoing = true;
		String board[][] = new String[9][9];
		if (p1.name == null || p2.name == null) {
			System.out.println("No players defined.\nPlease, configure the players");
			System.out.println();
			options();
		}
		startBoard(board);
		while (onGoing == true) {
			showBoard(board);
			int column = requestColumn();
			if (playerTurn == 1) {
				while (!board[2][column].equals("0")) {
					System.out.println("Please select another column. This has been occupied");
					column = requestColumn();
				}
				board[2][column] = p1.token;
			} else if (playerTurn == 2) {
				while (!board[2][column].equals("0")) {
					System.out.println("Please select another column. This has been occupied");
					column = requestColumn();
				}
				board[2][column] = p2.token;
			}
			placeToken(column, board);
			if (playerTurn == 1) {
				playerTurn = 2;
			} else {
				playerTurn = 1;
			}
			hasWon(board);
		}
		showBoard(board);
	}

	private static void hasWon(String[][] board) {
		// check horizontal
		for (int i = 2; i < 8; i++) {
			for (int j = 1; j < 5; j++) {
				if (board[i][j] == board[i][j + 1] && board[i][j] == board[i][j + 2] && board[i][j] == board[i][j + 3]
						&& !board[i][j].equals("0")) {
					System.out.println("VICTORY");
					System.out.println();
					onGoing = false;
				}
			}
		}
		// check vertical
		for (int i = 2; i < 5; i++) {
			for (int j = 1; j < 8; j++) {
				if (board[i][j] == board[i + 1][j] && board[i][j] == board[i + 2][j] && board[i][j] == board[i + 3][j]
						&& !board[i][j].equals("0")) {
					System.out.println("VICTORY");
					System.out.println();
					onGoing = false;
				}
			}
		}
		// check diagonal
		for (int i = 2; i < 5; i++) {
			for (int j = 1; j < 5; j++) {
				if (board[i][j] == board[i + 1][j + 1] && board[i][j] == board[i + 2][j + 2]
						&& board[i][j] == board[i + 3][j + 3] && !board[i][j].equals("0")) {
					System.out.println("VICTORY");
					System.out.println();
					onGoing = false;
				}
			}
		}
		// check diagonal
		for (int i = 2; i < 5; i++) {
			for (int j = 4; j < board.length; j++) {
				if (board[i][j] == board[i + 1][j - 1] && board[i][j] == board[i + 2][j - 2]
						&& board[i][j] == board[i + 3][j - 3] && !board[i][j].equals("0")) {
					System.out.println("VICTORY");
					System.out.println();
					onGoing = false;
				}
			}
		}
	}

	private static String[][] placeToken(int requestColumn, String board[][]) {
		for (int i = 2; i < 8; i++) {
			for (int j = 1; j < 8; j++) {
				if (board[i + 1][j].equals("0") && !board[i][j].equals("0")) {
					board[i + 1][j] = board[i][j];
					board[i][j] = "0";
					placeToken(requestColumn, board);
				}
			}
		}
		return board;
	}

	private static int requestColumn() {
		if (playerTurn == 1) {
			System.out.println("Turn: " + p1.name);
		} else {
			System.out.println("Turn: " + p2.name);
		}
		System.out.println("Where do you want to play?\nChose a column within the board");
		int column = a.nextInt();
		while (column > 7 || column < 1) {
			System.out.println("Please, chose a valid column");
			column = a.nextInt();
		}
		return column;
	}

	private static void showBoard(String[][] board) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static String[][] startBoard(String board[][]) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				board[i][j] = "0";
				if (i == 0) {
					board[i][j] = String.valueOf(j);
				}
				if (j == 0) {
					board[i][j] = "[";
				}
				if (j == board.length - 1) {
					board[i][j] = "]";
				}
				if (i == 1) {
					board[i][j] = "_";
				}
				if (i == 8) {
					board[i][j] = "-";
				}
			}
		}
		return board;
	}

	private static void options() {
		System.out.println(" - Options menu - ");
		System.out.println("Chose the name of the players, and which player will have each token");
		System.out.print("Player 1 name: ");
		a.nextLine();
		p1.name = a.nextLine();
		System.out.print("Player 2 name: ");
		p2.name = a.nextLine();
		System.out.println();
		System.out.print("Player 1 token: ");
		p1.token = a.nextLine();
		System.out.print("Player 2 token: ");
		p2.token = a.nextLine();
		System.out.println();
		System.out.println("Options configured successfully!");
	}

	private static void showMenu() {
		System.out.println();
		System.out.println("   1 -> Play game");
		System.out.println("   2 -> Options");
		System.out.println("   3 -> Quit");
		System.out.println("Your election: ");
	}

}
